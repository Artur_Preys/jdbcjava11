package repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import domain.Department;
import domain.Project;
import utils.HibernateUtils;

public class DepartmentHiberRepository {

   public Department findById(Integer deptId){
       Session session = HibernateUtils.getSessionFactory().openSession();
       Department dept = session.find(Department.class, deptId);
       session.close();
       return dept;
   }

   public List<Department> findAll(){
       Session session = HibernateUtils.getSessionFactory().openSession();
       String selectAllDeptsHQL = "from Department";
       Query<Department> selectAllDeptsQuery = session.createQuery(selectAllDeptsHQL, Department.class);
       List<Department> result = selectAllDeptsQuery.list();
       session.close();
       return result;
   }

   public List<Department> findByName(String depName){

       Session session = HibernateUtils.getSessionFactory().openSession();
       String selectAllDeptsHQL = "from Department d where d.name = :dName";
       Query<Department> selectAllDeptsQuery = session.createQuery(selectAllDeptsHQL, Department.class);
       selectAllDeptsQuery.setParameter("dName", depName);

       List<Department> result = selectAllDeptsQuery.list();
       session.close();
       return result;
   }


    public void save(Department department){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(department);
        transaction.commit();
        session.close();
    }

    public void update(Department department){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(department);
        transaction.commit();
        session.close();
    }

    public void delete(Department department){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(department);
        transaction.commit();
        session.close();
    }


    public List<Department> findAllByCriteria(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Department> cr = cb.createQuery(Department.class);
        Root<Department> root = cr.from(Department.class);
        cr.select(root);

        Query<Department> query = session.createQuery(cr);
        List<Department> result = query.getResultList();

        session.close();
        return result;

    }

    public List<Department> findAllByNameAndId(String deptName, Integer deptId){
        Session session = HibernateUtils.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Department> cr = cb.createQuery(Department.class);
        Root<Department> root = cr.from(Department.class);
        cr.select(root);

        cr.where(cb.and(cb.equal(root.get("name"), deptName), cb.equal(root.get("departmentId"), deptId)));
/// TO SQL ==> select *  from departments d where d.name = 'name' and d.departmentId = 'deptId'
        Query<Department> query = session.createQuery(cr);
        List<Department> result = query.getResultList();
        session.close();
        return result;

    }






}
