package repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import domain.Department;
import domain.Project;
import utils.HibernateUtils;

public class ProjectHiberRepository {


    public Project findById(Integer projectId){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Project project = session.find(Project.class, projectId);
        session.close();
        return project;
    }

    public List<Project> findAll(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllProjectsHQL = "from Project";
        Query<Project> selectAllProjectsQuery = session.createQuery(selectAllProjectsHQL, Project.class);
        List<Project> result = selectAllProjectsQuery.list();
        session.close();
        return result;
    }

    public void save(Project project){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(project);
        transaction.commit();
        session.close();
    }

    public void update(Project project){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(project);
        transaction.commit();
        session.close();
    }

    public void delete(Project project){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(project);
        transaction.commit();
        session.close();
    }

}
