package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Department;
import utils.DatabaseUtils;

public class DepartmentRepository {

    public List<Department> findAll(){

        List<Department> departments = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
              Statement statement = conn.createStatement();
              ResultSet result = statement.executeQuery("select * from departments");){
            while(result.next()){
                Integer deptId = result.getInt("departmentId");
                String deptName = result.getString("name");
                Department department = new Department(deptId, deptName);
                departments.add(department);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return departments;
    }

    public void changeDepartmentName(String nameToChange){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            Statement statement = conn.createStatement();)
        {
            String updateSql = "update departments set name = '"  + nameToChange + "' where departmentId = 1 ";
            int affectedRowcount = statement.executeUpdate(updateSql);
            System.out.println("rows affected: " + affectedRowcount);

        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public Department findById(Integer deptId){
        Department department= new Department();
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("select * from departments where departmentId = ?")
        )
        {
            stmt.setInt(1, deptId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                Integer departmentId = result.getInt("departmentId");
                String deptName = result.getString("name");
                department = new Department(departmentId, deptName);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return department;
    }


    public void deleteById(Integer deptId){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("delete from departments where departmentId = ?")
        )
        {
            stmt.setInt(1, deptId);
            int deletedRowCount = stmt.executeUpdate();
            System.out.println("Rows deleted: " + deletedRowCount);
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void save(Department department){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO departments(name) values(?)")
        )
        {
            stmt.setString(1, department.getName());
            int insertedRowCount = stmt.executeUpdate();
            System.out.println("Rows inserted: " + insertedRowCount);
        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }

    public void update(Department department){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("update departments set name = ? where departmentId=?")
        )
        {
            stmt.setString(1, department.getName());
            stmt.setInt(2,department.getDepartmentId());
            int affectedRows = stmt.executeUpdate();
            System.out.println("Rows affected: " + affectedRows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }

     public Department findByName(String departmentNameToFind){
         Department department= new Department();
         try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("select * from departments where name = ?")
         )
         {
             stmt.setString(1, departmentNameToFind);
             ResultSet result = stmt.executeQuery();
             while(result.next()){
                 Integer departmentId = result.getInt("departmentId");
                 String deptName = result.getString("name");
                 department = new Department(departmentId, deptName);
             }
         }catch (SQLException ex){
             ex.printStackTrace();
         }
         return department;
     }


}
