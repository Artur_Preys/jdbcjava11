package repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import domain.Department;
import domain.Employee;
import utils.HibernateUtils;

public class EmployeeHiberRepository {

    public Employee findById(Integer employeeId){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Employee employee = session.find(Employee.class, employeeId);
        session.close();
        return employee;
    }

    public List<Employee> findAll(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String findAllEmployeesHQL = "from Employee";
        Query<Employee> findAllQuery = session.createQuery(findAllEmployeesHQL,Employee.class);
        List<Employee> result = findAllQuery.list();
        session.close();
        return result;
    }

    public List<Employee> findAllWithNameStartsWith(String startName){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String findByNameStartsHQL = "from Employee e where e.firstName like :startingName";
        Query<Employee> findAllByNameStartQuery = session.createQuery(findByNameStartsHQL,Employee.class);

        findAllByNameStartQuery.setParameter("startingName", startName+"%");
        List<Employee> result = findAllByNameStartQuery.list();
        session.close();
        return result;
    }

    public List<Employee> findEmployeesFromDepartment(String departmentName){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String findEmpByDeptsHQL = "from Employee e where e.department.name = :deptName";

        Query<Employee> findEmpByDeptsQuery = session.createQuery(findEmpByDeptsHQL,Employee.class);
        findEmpByDeptsQuery.setParameter("deptName", departmentName);

        List<Employee> result = findEmpByDeptsQuery.list();
        session.close();
        return result;
    }

    public List<Employee> findAllSortedByName(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String findAllEmployeesSortedHQL = "from Employee e order by e.firstName asc";
        Query<Employee> findAllSortedQuery = session.createQuery(findAllEmployeesSortedHQL,Employee.class);
        List<Employee> result = findAllSortedQuery.list();
        session.close();
        return result;
    }

    public List<Employee> findByDeparmentName(String departmentName){
        Session session = HibernateUtils.getSessionFactory().openSession();
        String findyByDeptHQL = "FROM Employee e where e.department.name = :deptName";
        Query<Employee> findByDeptQuery = session.createQuery(findyByDeptHQL,Employee.class);

        findByDeptQuery.setParameter("deptName", departmentName );
        List<Employee> result = findByDeptQuery.list();
        session.close();
        return result;
    }




    public void save(Employee employee){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();
    }

    public void update(Employee employee){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(employee);
        transaction.commit();
        session.close();
    }

    public void delete(Employee employee){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(employee);
        transaction.commit();
        session.close();
    }

    public List<Employee> findAllBySalaryAndPhone(String startsPhone, Integer minSalary){
        Session session = HibernateUtils.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
        Root<Employee> root = cr.from(Employee.class);
        cr.select(root);
        cr.where(cb.and(cb.greaterThan(root.get("salary"), minSalary ), cb.like(root.get("phoneNumber"), startsPhone+"%")));
        Query<Employee> query = session.createQuery(cr);
        List<Employee> result = query.getResultList();
        session.close();
        return result;

    }





}
