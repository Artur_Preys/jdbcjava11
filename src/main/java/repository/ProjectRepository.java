package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Project;
import utils.DatabaseUtils;

public class ProjectRepository {

    public List<Project> findAll(){
        List<Project> projects = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("select * from projects");){
            while(result.next()){
                Integer projId = result.getInt("projectId");
                String description = result.getString("description");
                Project project = new Project(projId, description);
                projects.add(project);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return projects;
    }


    public Project findById(Integer projectId){
        Project project= new Project();
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("select * from projects where projectId = ?")
        )
        {
            stmt.setInt(1, projectId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                Integer projId = result.getInt("projectId");
                String description = result.getString("description");
                project = new Project(projId, description);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return project;
    }


    public void deleteById(Integer projectId){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("delete from projects where projectId = ?")
        )
        {
            stmt.setInt(1, projectId);
            int deletedRowCount = stmt.executeUpdate();
            System.out.println("Rows deleted: " + deletedRowCount);
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void save(Project project){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO projects(description) values(?)")
        )
        {
            stmt.setString(1, project.getDescription());
            int insertedRowCount = stmt.executeUpdate();
            System.out.println("Rows inserted: " + insertedRowCount);
        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }

    public void update(Project project){
        try(Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("update projects set name = ? where projectId=?")
        )
        {
            stmt.setString(1, project.getDescription());
            stmt.setInt(2,project.getProjectId());
            int affectedRows = stmt.executeUpdate();
            System.out.println("Rows affected: " + affectedRows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }

}
