package utils;

public class DatabaseUtils {

    public static final String DATABASE_HOST = "jdbc:mysql://localhost:3306/humanResourcesJava11?serverTimezone=UTC";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "changeme";
}
