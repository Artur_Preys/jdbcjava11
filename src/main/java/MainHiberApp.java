import org.hibernate.Session;
import org.hibernate.Transaction;

import domain.Department;
import domain.Employee;
import domain.Project;
import repository.DepartmentHiberRepository;
import repository.EmployeeHiberRepository;
import repository.ProjectHiberRepository;
import utils.HibernateUtils;

public class MainHiberApp {

    public static void main(String[] args) {
      /*  Session session = HibernateUtils.getSessionFactory().openSession();
        Department dept = session.find(Department.class, 1);
        System.out.println(dept);

        Transaction transaction = session.beginTransaction();
        Department department = new Department();
        department.setName("New fresh department");
        session.save(department);
        transaction.commit();
        session.close();*/

     /*   DepartmentHiberRepository departmentHiberRepository = new DepartmentHiberRepository();
        Department dept = departmentHiberRepository.findById(1);
        System.out.println(dept);

        ProjectHiberRepository projectHiberRepository = new ProjectHiberRepository();
        Project project = projectHiberRepository.findById(2);
        System.out.println(project);

        Project sdaProject = new Project();
        sdaProject.setDescription("Our Sda hibernate project");
        projectHiberRepository.save(sdaProject);
*/

        DepartmentHiberRepository departmentHiberRepository = new DepartmentHiberRepository();

      /*  Department testDep = new Department();
        testDep.setName("Test sda hiber dept");

        departmentHiberRepository.save(testDep);

        Department deptFromDb = departmentHiberRepository.findById(8);

        deptFromDb.setName("Test sda hiber dept updated");
        departmentHiberRepository.update(deptFromDb);

        Department deptFromDb = departmentHiberRepository.findById(8);
        departmentHiberRepository.delete(deptFromDb);
         */

        EmployeeHiberRepository employeeHiberRepository = new EmployeeHiberRepository();
        Employee employee = employeeHiberRepository.findById(1);
        System.out.println(employee);
        System.out.println("Employee department:");
        System.out.println(employee.getDepartment());

        System.out.println("Employee 1 Manager is: ");
        System.out.println(employee.getManager());

        System.out.println("\n --------------- ");


        Department departmentFromDb = departmentHiberRepository.findById(1);
        System.out.println(departmentFromDb);
        System.out.println("Department 1 employee:");
        departmentFromDb.getEmployees().forEach(it-> System.out.println(it));

        System.out.println("\n --------------- ");
        ProjectHiberRepository projectHiberRepository = new ProjectHiberRepository();

        Project projFromDb =  projectHiberRepository.findById(1);

        System.out.println(projFromDb);
        System.out.println("project employees : ");
        projFromDb.getEmployees().forEach(it-> System.out.println(it));


        System.out.println("\n --------------- ");
        System.out.println("\n --------------- ");
        departmentHiberRepository.findAll().forEach(it-> System.out.println(it));

        departmentHiberRepository.findByName("HR").forEach(it-> System.out.println(it));


        employeeHiberRepository.findByDeparmentName("HR").forEach(it-> System.out.println(it));


        System.out.println("\n Display all projects ");
        projectHiberRepository.findAll().forEach(it-> System.out.println(it));

        System.out.println("\n Display all employees ");
        employeeHiberRepository.findAll().forEach(it-> System.out.println(it));

        System.out.println("\n Display all employees with name Starting with Letter J");
        employeeHiberRepository.findAllWithNameStartsWith("J").forEach(it-> System.out.println(it));

        System.out.println("\n  Display all employees working in the Finance department");
        employeeHiberRepository.findByDeparmentName("Finance").forEach(it-> System.out.println(it));


        System.out.println("\n   Display all employees alphabetically");
        employeeHiberRepository.findAllSortedByName().forEach(it-> System.out.println(it));



        departmentHiberRepository.findAllByCriteria().forEach(it-> System.out.println(it));

        departmentHiberRepository.findAllByNameAndId("HR", 1).forEach(it-> System.out.println(it));



        System.out.println("\n   Display all employees with salaray and phone");
        employeeHiberRepository.findAllBySalaryAndPhone("0-800", 1800).forEach(it-> System.out.println(it));


    }
}
