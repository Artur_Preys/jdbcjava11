import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.result.SqlDateValueFactory;

import utils.DatabaseUtils;

public class MainApp {


    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(DatabaseUtils.DATABASE_HOST,DatabaseUtils.DATABASE_USERNAME,DatabaseUtils.DATABASE_PASSWORD);
            System.out.println(conn.getAutoCommit());

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery("select * from departments");

            while(result.next()){
                Integer deptId = result.getInt("departmentId");
                String deptName = result.getString("name");
                System.out.println(deptId + " " + deptName);
            }

            // All project
            System.out.println("\nAll projects");
            result = stmt.executeQuery("select * from projects");
            while(result.next()){
                Integer projectId = result.getInt("projectId");
                String description = result.getString("description");
                System.out.println(projectId + " " + description);
            }

            // All employees
            System.out.println("\nAll employees");
            result = stmt.executeQuery("select * from employees");
            printEmployees(result);

            //All employees with names starting with the letter J

            System.out.println("\nAll employees with names starting with the letter J");
            result = stmt.executeQuery("select * from employees where firstName like 'J%'");
            printEmployees(result);

            //All employees that haven’t been assigned to a department

            System.out.println("\nAll employees that haven’t been assigned to a department");
            result = stmt.executeQuery("select * from employees where departmentId is null");
            printEmployees(result);

            //Display all employees along with the department they’re in
            System.out.println("\nDisplay all employees along with the department they’re in");
            result = stmt.executeQuery("select e.employeeId, e.firstName, e.lastName, e.departmentId, e.dateOfBirth, d.name "
                + "from employees e LEFT JOIN departments d ON  e.departmentId = d.departmentId");
            printEmployees(result);

            result.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void printEmployees(ResultSet result) throws SQLException {
        while(result.next()){
            String departmentName = "";
            Integer employeeId = result.getInt("employeeId");
            String firstName = result.getString("firstName");
            String lastName = result.getString("lastName");
            Date dateOfBirth = result.getDate("dateOfBirth");
            Integer departmentId = result.getInt("departmentId");
            try {
                departmentName = result.getString("name");
            } catch (SQLException ex){}
            System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth + " " + departmentId  + " " + departmentName);
        }
    }
}
